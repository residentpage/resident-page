package com.ios.iosgate;

import android.content.Intent;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;

public class ContentActivity extends AppCompatActivity {

    static final int REQUEST_IMAGE_CAPTURE=1;

    private Button buttonsignin;
    public EditText signinemail;
    public EditText signinpassword;

    private FirebaseAuth firebaseAuth;

    @Override
    protected void onCreate (@Nullable Bundle savedInstanceState) {
        super.onCreate( savedInstanceState );

        setContentView( R.layout.activity_content );

        signinemail = (EditText) findViewById( R.id.signinemail );
        signinpassword = (EditText) findViewById( R.id.signinpassword );

        firebaseAuth = FirebaseAuth.getInstance();

        buttonsignin = findViewById( R.id.buttonsignin );
        buttonsignin.setOnClickListener( new View.OnClickListener() {
            @Override
            public void onClick (View v) {
                String userEmail = signinemail.getText().toString();
                String userPassword = signinpassword.getText().toString();

                signinTheUserWithEmailAndPassword( userEmail, userPassword );

            }
        } );

    }
        private void signinTheUserWithEmailAndPassword (String userEmail, String userPassword){

            firebaseAuth.signInWithEmailAndPassword( userEmail, userPassword )
                    .addOnCompleteListener( new OnCompleteListener<AuthResult>() {
                        @Override
                        public void onComplete (@NonNull Task<AuthResult> task) {

                            if (!task.isSuccessful()) {

                                Toast.makeText( ContentActivity.this, "Error", Toast.LENGTH_SHORT ).show();
                            } else {

                                Intent signinintent = new Intent( ContentActivity.this, SendlinkActivity.class );
                                startActivity( signinintent );

                            }

                        }
                    } );



    }
        public void onClickSignup (View v) {

        Intent signupintent= new Intent( this,SignupActivity.class );


//        signupintent.putExtra( "name", "Reenath" );

        startActivity( signupintent );


    }

    public void launchCamera (View view) {
        Intent intent = new Intent( MediaStore.ACTION_IMAGE_CAPTURE );
        intent.putExtra("android.intent.extras.CAMERA_FACING", 1);

        startActivityForResult( intent,REQUEST_IMAGE_CAPTURE );

            }

    @Override
    protected void onActivityResult (int requestCode, int resultCode, Intent data) {
        super.onActivityResult( requestCode, resultCode, data );
    }



    public void onClickskip (View view) {
        Intent skipintent= new Intent( this, SendlinkActivity.class);
        startActivity( skipintent );
    }
}






