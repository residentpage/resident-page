package com.ios.iosgate;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Build;
import android.os.Environment;
import android.os.StrictMode;
import android.provider.ContactsContract;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.annotation.RequiresApi;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.SurfaceView;
import android.view.TextureView;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;
import static android.Manifest.permission.CAMERA;
import static java.security.AccessController.getContext;

import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.concurrent.TimeUnit;

import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;

public class Securitypage extends AppCompatActivity {

    //    private Button securitybutton;
    private ImageButton visitorimage;
    private final int WRITE_EXTERNAL_STORAGE = 1;
    private final int REQUEST_WRITE_PERMISSION = 1;
    public static final int REQUEST_CAMERA = 0;
    final int MY_PERMISSIONS_REQUEST_CAMERA=1;
    final int PERMISSION_REQUEST_CODE=1;



    private TextView flat;
    private EditText Flatno;

    public ListView userlist;
    public ArrayList<String> visitordatails;

    private EditText visitorName;
    private ProgressDialog mProgressDialog;

    private File File;

    int mob_visitor = 1;

    public String local_img_path;
    public TextView api_resp_view;
    public String myApi = "http://production.vishwamcorp.com/v2/face_lookup_ios";
    public int RespCode;
    public String username = "suri888";
    public String rr,resp;


    private FirebaseStorage firebaseStorage;
    private StorageReference storageReference;

    @Override
    protected void onCreate (Bundle savedInstanceState) {
        super.onCreate( savedInstanceState );
        setContentView( R.layout.activity_securitypage );

        StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();

        StrictMode.setThreadPolicy( policy );


//        api_resp_view = findViewById(R.id.api_resp_view);


        mProgressDialog = new ProgressDialog( this );

        visitorimage = (ImageButton) findViewById( R.id.visitorimage );


        firebaseStorage = FirebaseStorage.getInstance();
        storageReference = firebaseStorage.getReferenceFromUrl( "gs://iosgate-4ebaf.appspot.com" );

        //To open camera
        visitorimage.setOnClickListener( new View.OnClickListener() {
            @RequiresApi(api = Build.VERSION_CODES.M)

            @Override
            public void onClick (View v) {
//                Intent cameraintent = new Intent( Intent.ACTION_PICK );
//
                Intent cameraintent = new Intent( MediaStore.ACTION_IMAGE_CAPTURE );
                cameraintent.putExtra( "android.intent.extras.CAMERA_FACING", 1 );
                cameraintent.putExtra( "name", "responseview" );

                startActivityForResult( cameraintent, 1 );


            }

        } );

    }
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        JSONObject respBody = new JSONObject();
        //after image is captured, Take the image, store it in the local path


        if (requestCode == 1 && resultCode == RESULT_OK) {
            if (data != null && data.getExtras() != null) {

                Uri imageURI = data.getData();

                // Taking the image as bitmap from the camera

                Bitmap imageBitmap = (Bitmap) data.getExtras().get("data");

                //mImageView.setImageBitmap(imageBitmap);

                //store the image locally in a directory

                File file = getOutputMediaFile();

                try {


                    FileOutputStream fileOutputStream = new FileOutputStream(file);

                    imageBitmap.compress(Bitmap.CompressFormat.JPEG, 90, fileOutputStream);
                    fileOutputStream.flush();
                    fileOutputStream.close();
                    uploadPicture ();


                } catch (FileNotFoundException e) {
                    e.printStackTrace();
                } catch (IOException e) {
                    e.printStackTrace();
                }


            }
        }

        //Now Take the picture from local file system and hit the api end point

        final File imageFile1 = new File(local_img_path);
        OkHttpClient client = new OkHttpClient.Builder()
                .connectTimeout(40, TimeUnit.SECONDS)
                .build();


        MultipartBody.Builder multipartBodyBuilder = new MultipartBody.Builder();
        multipartBodyBuilder.setType(MultipartBody.FORM); //this may not be needed

        multipartBodyBuilder.addFormDataPart("user_id", username);
        multipartBodyBuilder.addFormDataPart("image", imageFile1.getName(), RequestBody.create( MediaType.parse("image/jpg"), imageFile1));
        // multipartBodyBuilder.addFormDataPart("gesture", String.valueOf(gesture1Number));
        //multipartBodyBuilder.addFormDataPart("landmarks", landmarks1);
        //multipartBodyBuilder.addFormDataPart("delay", "500");
        //multipartBodyBuilder.addFormDataPart("version", "1");
        //multipartBodyBuilder.addFormDataPart("n", "1");

        Request request = new Request.Builder()
                .url(myApi)
                .post(multipartBodyBuilder.build())
                .build();


        Response response1 = null;
        try {
            response1 = client.newCall(request).execute();

            //Log.e("reenath", String.valueOf(response1));
            RespCode = response1.code();



            if(RespCode == 200){

                rr =  response1.body().string();

                JSONObject haha = new JSONObject(rr);
                resp = haha.getString("userId");


                Intent successintent=new Intent( Securitypage.this,Auth.class );

                successintent.putExtra("rr2",resp);
//                successintent.putExtra( "flat",flatnum );
                startActivity( successintent );
//                api_resp_view.setText(rr2);
                //rc.setText(rr2);
                FirebaseDatabase database = FirebaseDatabase.getInstance();
                final DatabaseReference myRef = database.getReferenceFromUrl( "https://iosgate-4ebaf.firebaseio.com/Guests" );

//                userlist=(ListView)findViewById( R.id.userlist );
//                ArrayAdapter<String> arrayAdapter=new ArrayAdapter<String>(this,android.R.layout.simple_list_item_1,visitordatails );



            }else {

                Intent failintent=new Intent( Securitypage.this,Unauth.class );

//                failintent.putExtra("rc","visitor is un-authorized");
                startActivity( failintent );
//
//                api_resp_view.setText("visitor is un-authorized");
            }


//            Intent resp_intent = new Intent();
//            resp_intent.setClass(this,Auth.class);
//            resp_intent.putExtra("rc",RespCode);


        } catch (IOException e) {
            e.printStackTrace();
        } catch (JSONException e) {
            e.printStackTrace();
        }


    }



    public File getOutputMediaFile(){

        final String TAG = "CameraPreview";

        File mediaStorageDir =
                new File( Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES), "VisitorImages");

        // Create the storage directory if it does not exist
        if (!mediaStorageDir.exists()) {
            if (!mediaStorageDir.mkdirs()) {
                // Log.d(TAG, "failed to create directory");
                return null;
            }
        }



        File file = new File(mediaStorageDir.getPath() + File.separator  + "_deltaTime1_" + "visitorName"  + ".jpg");

        local_img_path = file.getPath();

        return file;

    }


//            }
//        } );
//    }
//
//    @Override
//    protected void onActivityResult (int requestCode, int resultCode, Intent data) {
//
//        if (resultCode == RESULT_OK && requestCode == REQUEST_WRITE_PERMISSION) {
//
//
//            if (data != null && data.getExtras() != null) {
//                Uri imageURI = data.getData();
//
//                Bitmap bitmap = (Bitmap) data.getExtras().get( "data" );
//
//                File file = getOutputMediaFile();
//
//                try {
//
//                    FileOutputStream fileOutputStream = new FileOutputStream( file );
//
//                    bitmap.compress( Bitmap.CompressFormat.JPEG, 90, fileOutputStream );
//                    fileOutputStream.flush();
//                    fileOutputStream.close();
//                    uploadPicture();
//
//
//                } catch (FileNotFoundException e) {
//                    e.printStackTrace();
//                } catch (IOException e) {
//                    e.printStackTrace();
//                }
//
//            }
//        }
//    }
//
//    public String path1;
//
//
//    public File getOutputMediaFile () {
//
//        final String TAG = "CameraPreview";
//
//        File mediaStorageDir =
//                new File( Environment.getExternalStoragePublicDirectory( Environment.DIRECTORY_PICTURES ), "VisitorImages" );
//
//        // Create the storage directory if it does not exist
//        if (!mediaStorageDir.exists()) {
//            if (!mediaStorageDir.mkdirs()) {
//                // Log.d(TAG, "failed to create directory");
//                return null;
//            }
//        }
//
//        File file = new File( mediaStorageDir.getPath() + File.separator + "visitorName" + ".jpg" );
//
//        path1 = file.getPath();
//
//        return file;
//    }

    private void uploadPicture () {


        File file = new File( local_img_path );

        Uri uploadImage = Uri.fromFile( file );

        StorageReference filepath = storageReference.child( "visitorimage/Visitor" + mob_visitor );

        mob_visitor++;

        UploadTask uploadTask = filepath.putFile( uploadImage );

        uploadTask.addOnFailureListener( new OnFailureListener() {
            @Override
            public void onFailure (@NonNull Exception e) {

                Log.e( "fail", "fail" );

            }
        } ).addOnSuccessListener( new OnSuccessListener<UploadTask.TaskSnapshot>() {
            @Override
            public void onSuccess (UploadTask.TaskSnapshot taskSnapshot) {


                Log.e( "success", "success" );
            }
        } );
    }
}











//        StorageReference filepath = storageReference.child("Visitor");
//
//        mProgressDialog.setMessage("Uploading.....");
//        mProgressDialog.show();
//
//        filepath.child("VisitorImage").putFile(Uri.fromFile(File)).addOnSuccessListener(new OnSuccessListener<UploadTask.TaskSnapshot>() {
//            @Override
//            public void onSuccess(UploadTask.TaskSnapshot taskSnapshot) {
////                Uri downloadUri = taskSnapshot.getDownloadUrl();
//                Log.d("visitor","success upload small file" + visitorName);
//            }
//        }).addOnFailureListener(new OnFailureListener() {
//            @Override
//            public void onFailure(@NonNull Exception e) {
//                Toast.makeText(Securitypage.this,"Upload Failed..",Toast.LENGTH_LONG).show();
//            }
//        });
//
//        filepath.putFile(Uri.fromFile(File)).addOnSuccessListener(new OnSuccessListener<UploadTask.TaskSnapshot>() {
//            @Override
//            public void onSuccess(UploadTask.TaskSnapshot taskSnapshot) {
////                downloadUri = taskSnapshot.getDownloadUrl();
//                getOutputMediaFile();
//            }
//        }).addOnFailureListener(new OnFailureListener() {
//            @Override
//            public void onFailure(@NonNull Exception e) {
//                Toast.makeText(Securitypage.this,"Upload Failed..",Toast.LENGTH_LONG).show();
//            }
//        });
//
//
//
//                mProgressDialog.setMessage("Uploading.....");
//        mProgressDialog.show();
//
//        filepath.child("VisitorImage").putFile(Uri.fromFile(File)).addOnSuccessListener(new OnSuccessListener<UploadTask.TaskSnapshot>() {
//            @Override
//            public void onSuccess(UploadTask.TaskSnapshot taskSnapshot) {
////                Uri downloadUri = taskSnapshot.getDownloadUrl();
//                Log.d("visitor","success upload small file" + visitorName);
//            }
//        }).addOnFailureListener(new OnFailureListener() {
//            @Override
//            public void onFailure(@NonNull Exception e) {
//                Toast.makeText(Securitypage.this,"Upload Failed..",Toast.LENGTH_LONG).show();
//            }
//        });
//
//        filepath.putFile(Uri.fromFile(File)).addOnSuccessListener(new OnSuccessListener<UploadTask.TaskSnapshot>() {
//            @Override
//            public void onSuccess(UploadTask.TaskSnapshot taskSnapshot) {
////                downloadUri = taskSnapshot.getDownloadUrl();
//                getOutputMediaFile();
//            }
//        }).addOnFailureListener(new OnFailureListener() {
//            @Override
//            public void onFailure(@NonNull Exception e) {
//                Toast.makeText(Securitypage.this,"Upload Failed..",Toast.LENGTH_LONG).show();
//            }
//        });



//                    StorageReference imageAddressInFirebase = storageReference.child( "Images" )
//                            .child( imageURI.getLastPathSegment() );
//                    imageAddressInFirebase.putFile( imageURI ).addOnSuccessListener( new OnSuccessListener<UploadTask.TaskSnapshot>() {
//                        @Override
//                        public void onSuccess (UploadTask.TaskSnapshot taskSnapshot) {
//
//                            Toast.makeText( Securitypage.this,"Uploaded Successfully",Toast.LENGTH_LONG ).show();
//
//                        }
//                    } ).addOnFailureListener( new OnFailureListener() {
//                        @Override
//                        public void onFailure (@NonNull Exception e) {
//
//                            Toast.makeText( Securitypage.this,"Uploading Error.Please try again",Toast.LENGTH_LONG ).show();
//
//                        }
//                    } );
//                }




