package com.ios.iosgate;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageButton;

public class Homepage extends AppCompatActivity {

    private ImageButton securitybutton;
    private ImageButton residentbutton;

    @Override
    protected void onCreate (Bundle savedInstanceState) {
        super.onCreate( savedInstanceState );
        setContentView( R.layout.activity_homepage );
//
//        securitybutton = (ImageButton) findViewById( R.id.securitybutton );
//
//        securitybutton.setOnClickListener( new View.OnClickListener() {
//            @Override
//            public void onClick (View v) {
//                Intent securityintent = new Intent( Homepage.this, Securitypage.class );
//                startActivity( securityintent );
//            }
//        } );
//
//
//        residentbutton.setOnClickListener( new View.OnClickListener() {
//            @Override
//            public void onClick (View v) {
//                Intent residentintent =new Intent( Homepage.this,ContentActivity.class );
//                startActivity( residentintent );
//            }
//        } );
//    }


    }

    public void onClickresident (View view) {
        Intent residentintent = new Intent( this,ContentActivity.class );
        startActivity( residentintent );


    }

    public void onClicksecurity (View view) {
        Intent securityintent = new Intent( this,Securitypage.class );
        startActivity( securityintent );
    }
}
