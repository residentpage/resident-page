package com.ios.iosgate;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;

public class Smsfail extends AppCompatActivity {

    @Override
    protected void onCreate (Bundle savedInstanceState) {
        super.onCreate( savedInstanceState );
        setContentView( R.layout.activity_smsfail );

            }

    public void onClickback (View view) {
        Intent intent=new Intent(this,SendlinkActivity.class );
        startActivity( intent );
    }

    public void onClick (View view) {
        Intent intent=new Intent(this,MainActivity.class );
        startActivity( intent );
    }
}
