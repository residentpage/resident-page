package com.ios.iosgate;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;

public class SigninActivity extends AppCompatActivity{

    SharedPreferences log;
    Button login;

    @Override
    protected void onCreate (@Nullable Bundle savedInstanceState) {
        super.onCreate( savedInstanceState );

        setContentView( R.layout.activity_signin );

        login =findViewById( R.id.buttonsignin );



        log = getSharedPreferences("login",MODE_PRIVATE);


        if(log.getBoolean("logged",false)){
            goToMainActivity();
        }

        login.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                goToMainActivity();
                log.edit().putBoolean("logged",true).apply();
            }
        });
    }

    public void goToMainActivity(){
        Intent i = new Intent(this,MainActivity.class);
        startActivity(i);
    }
}
