package com.ios.iosgate;

import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.auth.UserProfileChangeRequest;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;

public class SignupActivity extends AppCompatActivity {

    static final int REQUEST_IMAGE_CAPTURE = 1;
    int mob_visitor = 1;

    public static String name = "shashank";
    EditText signupname;
    public EditText signupemail;
    private EditText signupmobile;
    private EditText signupflat;
    public EditText signuppassword;
    private TextView reenath;

    private ImageButton buttonuploadimage;
    private Button buttonsubmit;
    private Button buttonsignin;

    private FirebaseAuth firebaseAuth;
    private StorageReference mStorageRef;
    private FirebaseStorage firebaseStorage;
    private StorageReference storageReference;


    @Override
    protected void onCreate (@Nullable final Bundle savedInstanceState) {
        super.onCreate( savedInstanceState );

        setContentView( R.layout.activity_signup );

        Intent intent = getIntent();

        String gg = intent.getStringExtra( "name" );

//
//
//       Log.e( "reenath", gg );

        signupname = (EditText) findViewById( R.id.signupname );
        signupemail = (EditText) findViewById( R.id.signupemail );
        signupmobile = (EditText) findViewById( R.id.signupmobile );
        signupflat = (EditText) findViewById( R.id.signupflat );
        signuppassword = (EditText) findViewById( R.id.signuppassword );

//        reenath = (TextView)findViewById( R.id.reenath );
//        reenath.setText( gg );
        //  buttonsignin = findViewById( R.id.buttonsubmit );

//        buttonuploadimage=(ImageButton)findViewById( R.id.buttonuploadimage );
        buttonsubmit = (Button) findViewById( R.id.buttonsubmit );

        firebaseAuth = FirebaseAuth.getInstance();

        FirebaseDatabase database = FirebaseDatabase.getInstance();
        final DatabaseReference myRef = database.getReferenceFromUrl( "https://iosgate-4ebaf.firebaseio.com/Residents" );

        buttonsubmit.setOnClickListener( new View.OnClickListener() {
            @Override
            public void onClick (View v) {

                //to Signup
                name = signupname.getText().toString();
                String userEmail = signupemail.getText().toString();
                String userPassword = signuppassword.getText().toString();

                signupUserWithEmailAndPassword( userEmail, userPassword );

                //To store data into Database

//                myRef.child( "Residents" ).child( signupmobile.getText().toString() ).child( "name" ).setValue( signupname.getText().toString() );
//                myRef.child( "Residents" ).child( signupmobile.getText().toString() ).child( "mobilenumber" ).setValue( signupflat.getText().toString() );
//                myRef.child( "Residents" ).child( signupmobile.getText().toString() ).child( "emailid" ).setValue( signupemail.getText().toString() );
//                myRef.child( "Residents" ).child( signupmobile.getText().toString() ).child( "flatnumber" ).setValue( signupflat.getText().toString() );

                myRef.child( signupmobile.getText().toString() ).child( "name" ).setValue( signupname.getText().toString() );
                myRef.child( signupmobile.getText().toString() ).child( "mobilenumber" ).setValue( signupflat.getText().toString() );
                myRef.child( signupmobile.getText().toString() ).child( "emailid" ).setValue( signupemail.getText().toString() );
                myRef.child( signupmobile.getText().toString() ).child( "flatnumber" ).setValue( signupflat.getText().toString() );


//                if (savedInstanceState!=null){
//                    Toast.makeText( getApplicationContext(), "Successfully Submited", Toast.LENGTH_SHORT ).show();
//                }else {
//                    Toast.makeText( getApplicationContext(),"Please fill all deatils",Toast.LENGTH_SHORT ).show();
//                }

            }
        } );


    }


    public void onClickimagesignup (View view) {

        Intent intent = new Intent( MediaStore.ACTION_IMAGE_CAPTURE );


        intent.putExtra( "android.intent.extras.CAMERA_FACING", 1 );
        startActivityForResult( intent, REQUEST_IMAGE_CAPTURE );

        mStorageRef = FirebaseStorage.getInstance().getReference();

    }

    @Override
    protected void onActivityResult (int requestCode, int resultCode, Intent data) {


        if (data != null && data.getExtras() != null) {
            Uri imageURI = data.getData();

            Bitmap bitmap = (Bitmap) data.getExtras().get( "data" );

            File file = getOutputMediaFile();

            try {

                FileOutputStream fileOutputStream = new FileOutputStream( file );

                bitmap.compress( Bitmap.CompressFormat.JPEG, 90, fileOutputStream );
                fileOutputStream.flush();
                fileOutputStream.close();
                uploadPicture();


            } catch (FileNotFoundException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }

        }
    }



    public String path1;


    public File getOutputMediaFile () {

        final String TAG = "CameraPreview";

        File mediaStorageDir =
                new File( Environment.getExternalStoragePublicDirectory( Environment.DIRECTORY_PICTURES ), "Resident" );

        // Create the storage directory if it does not exist
        if (!mediaStorageDir.exists()) {
            if (!mediaStorageDir.mkdirs()) {
                // Log.d(TAG, "failed to create directory");
                return null;
            }
        }


        File file = new File( mediaStorageDir.getPath() +File.separator + signupname + ".jpg" );

        path1 = file.getPath();

        return file;
    }
    private void uploadPicture (){


        File file = new File( path1 );

        Uri uploadImage = Uri.fromFile( file );

        StorageReference filepath = storageReference.child( "Resident"+ signupname + mob_visitor );

        mob_visitor++;

        UploadTask uploadTask = filepath.putFile( uploadImage );

        uploadTask.addOnFailureListener( new OnFailureListener() {

        @Override
        public void onFailure (@NonNull Exception e) {

                Log.e( "fail", "fail" );

                }
                } ).addOnSuccessListener( new OnSuccessListener<UploadTask.TaskSnapshot>() {
        @Override
        public void onSuccess (UploadTask.TaskSnapshot taskSnapshot) {


                Log.e( "success", "success" );
        }
        } );
    }

    //User Signup
    private void signupUserWithEmailAndPassword (String userEmail, String userPassword) {

        firebaseAuth.createUserWithEmailAndPassword( userEmail, userPassword )
                .addOnCompleteListener( SignupActivity.this,
                        new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete (@NonNull Task<AuthResult> task) {


                        if(!task.isSuccessful()){
                            Toast.makeText( SignupActivity.this,"Error.Try again" ,Toast.LENGTH_SHORT).show();
                        }else {


                            SharedPreferences.Editor editor = getSharedPreferences("shanksPrefs", MODE_PRIVATE).edit();
                            editor.putString("SignupName",name );
                            editor.apply();

                            Toast.makeText( SignupActivity.this,"Successfully Registered",Toast.LENGTH_SHORT ).show();

                            Intent intent=new Intent( SignupActivity.this,ContentActivity.class );
                            startActivity( intent );
                            specifyUserProfile();


                        }

                    }
                } );

    }

    private void specifyUserProfile(){
        FirebaseUser firebaseUser=FirebaseAuth.getInstance().getCurrentUser();

        if (firebaseUser!=null){
            UserProfileChangeRequest userProfileChangeRequest=new UserProfileChangeRequest.Builder()
                    .setDisplayName( signupname.getText() .toString()).build();

            firebaseUser.updateProfile(userProfileChangeRequest)
                    .addOnCompleteListener( new OnCompleteListener<Void>() {
                @Override
                public void onComplete (@NonNull Task<Void> task) {
                    if (task.isSuccessful()){

                        //Done


                    }else{
                        //Error

                    }

                }
            } );
        }
    }


}





